import React from 'react'

import cx from 'classnames'

class Collapsible extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      collapsed: true
    }
  }

  toggle() {
    this.setState({
      collapsed: ! this.state.collapsed
    })
  }

  render() {
    return (
      <div className={cx('collapsible', { collapsed: this.state.collapsed })}>
        <div className="contents">
          {this.props.children}
        </div>

        <button className="btn btn-link" onClick={() => this.toggle()}>
          {this.state.collapsed ?'Show more...' :'Hide'}
        </button>
      </div>    
    )
  }
}

export default Collapsible