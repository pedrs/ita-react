import React from 'react'

const CustomerForm = ({ customer, handleChange  }) => {

  return (
    <form>
       <div className="row"> 
        <div className="col-md-6">
          <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" className="form-control" value={customer.name} onChange={handleChange} />
          </div>
          <div className="form-group">
            <label>Phone</label>
            <input name="phone" type="text" className="form-control" value={customer.phone} onChange={handleChange} />
          </div>
        </div>
        <div className="col-md-6">
          <div class="form-group">
            <label>Email</label>
            <input name="email" type="email" className="form-control" value={customer.email} onChange={handleChange} />
          </div>
          <div className="form-group">
            <label>City</label>
            <input name="city" type="text" className="form-control" value={customer.city} onChange={handleChange} />
          </div>
        </div>
        <div className="col-md-12">
          <div class="form-group">
            <label>Note</label>
            <textarea name="note" className="form-control" value={customer.note} onChange={handleChange} />
          </div>
        </div>
      </div>
    </form>
  )
}

export default CustomerForm
